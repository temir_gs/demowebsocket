package com.notification.ms.eventDispatchers;

import com.notification.ms.models.DemoMessage;
import com.notification.ms.models.RabbitConfigurationProperties;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.stereotype.Component;

@Component
public class EventDispatcher {

    private final AmqpTemplate amqpTemplate;
    private final RabbitConfigurationProperties configurationProperties;

    public EventDispatcher(AmqpTemplate amqpTemplate, RabbitConfigurationProperties configurationProperties) {
        this.amqpTemplate = amqpTemplate;
        this.configurationProperties = configurationProperties;
    }

    public void baseDispatch(DemoMessage demoMessage) {
        this.publish(demoMessage, configurationProperties.getExchange(), configurationProperties.getRoutingKey());
    }

    private void publish(Object payload, String exchange, String routingKey) {
        amqpTemplate.convertAndSend(exchange, routingKey, payload);
        System.out.println("Send msg = " + payload);
    }
}
