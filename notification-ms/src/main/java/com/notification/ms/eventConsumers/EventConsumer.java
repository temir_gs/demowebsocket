package com.notification.ms.eventConsumers;

import com.notification.ms.hubs.NotificationHub;
import com.notification.ms.models.DemoMessage;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class EventConsumer {
    private final NotificationHub notificationHub;

    public EventConsumer(NotificationHub notificationHub) {
        this.notificationHub = notificationHub;
    }

    @RabbitListener(queues = "demo.consume.queue")
    public void handleMessage(DemoMessage demoMessage) {
        System.out.println("Received Message: " + demoMessage);
        notificationHub.sendToUsers(1, "/queue/messages", demoMessage);
    }
}
