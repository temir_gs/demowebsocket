package com.notification.ms.controllers;

import com.notification.ms.eventDispatchers.EventDispatcher;
import com.notification.ms.models.DemoMessage;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.user.SimpUserRegistry;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class DemoController {
    private final EventDispatcher eventDispatcher;
    private final SimpUserRegistry simpUserRegistry;

    public DemoController(EventDispatcher eventDispatcher, SimpUserRegistry simpUserRegistry) {
        this.eventDispatcher = eventDispatcher;
        this.simpUserRegistry = simpUserRegistry;
    }

    @PostMapping("/send")
    public void sendMessage(@RequestBody DemoMessage demoMessage) {
        eventDispatcher.baseDispatch(demoMessage);
    }

    @MessageMapping("/chat")
    public void demo(@Payload DemoMessage demoMessage, Principal principal) {
        eventDispatcher.baseDispatch(demoMessage);
    }
}