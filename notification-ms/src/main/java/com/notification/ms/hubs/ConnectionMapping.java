package com.notification.ms.hubs;

import java.util.*;
import java.util.stream.Collectors;

public class ConnectionMapping<T, K> {
    private final Map<T, K> connections = new HashMap<>();

    public void add(T tenantKey, K userKey) {
        connections.put(tenantKey, userKey);
    }

    public void remove(T tenantKey, K userKey) {
        connections.remove(tenantKey, userKey);
    }

    public List<K> getUserConnections(T tenantKey) {
        return connections.entrySet().stream().filter(e -> Objects.equals(e.getKey(), tenantKey))
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());
    }
}
