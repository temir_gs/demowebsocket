package com.notification.ms.hubs;

import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import java.util.List;

@Component
public class NotificationHub {
    private final SimpMessagingTemplate messagingTemplate;
    private final static ConnectionMapping<Integer, String> connections = new ConnectionMapping<>();

    public NotificationHub(SimpMessagingTemplate messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }

    @EventListener
    private void handleWebSocketConnectListener(SessionConnectedEvent event) {
        connections.add(1, event.getUser().getName());
    }

    @EventListener
    private void handleWebSocketDisconnectListener(SessionDisconnectEvent event) {
        connections.remove(1, event.getUser().getName());
    }

    public void sendToUsers(Integer tenantId, String destination, Object message) {
        List<String> _connections = connections.getUserConnections(tenantId);
        for (String userId : _connections) {
            messagingTemplate.convertAndSendToUser(userId, destination, message);
        }
    }

    public void sendToUser(Integer userId, String destination, Object message) {
        messagingTemplate.convertAndSendToUser(userId.toString(), destination, message);
    }
}
